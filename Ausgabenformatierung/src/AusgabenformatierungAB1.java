
public class AusgabenformatierungAB1 
{

	public static void main(String[] args) 
	{
		//Nr1 part one
		System.out.println("Nr.1");
		System.out.println("Das ist ein Beispielsatz. Ein Beispielsatz ist das.");
		
		//Nr1 part two
		System.out.printf("\nDas ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.\n\n");
		
		//Nr2
		System.out.println("Nr.2");
		System.out.printf("%7.1s\n","*************");
		System.out.printf("%8.3s\n","*************");
		System.out.printf("%9.5s\n","*************");
		System.out.printf("%10.7s\n","*************");
		System.out.printf("%11.9s\n","*************");
		System.out.printf("%12.11s\n","*************");
		System.out.printf("%13s\n","*************");
		System.out.printf("%8.3s\n","*************");
		System.out.printf("%8.3s\n\n","*************");
		System.out.println("Nr.3");
		System.out.printf("%.2f\n",22.4234234);
		System.out.printf("%.2f\n",111.2222);
		System.out.printf("%.2f\n",4.0);
		System.out.printf("%.2f\n",1000000.551);
		System.out.printf("%.2f\n",97.34);
	}
}
