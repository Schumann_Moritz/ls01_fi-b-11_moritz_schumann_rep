﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {  
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
       
       zuZahlenderBetrag= fahrkartenBestellungErfassen();
       eingezahlterGesamtbetrag= fahrkartenBezahlen(zuZahlenderBetrag);// Geldeinwurf
       fahrkartenAusgeben("\nFahrschein wird ausgegeben");// Fahrscheinausgabe  
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; // Rückgeldberechnung und -Ausgabe
       rueckgeldAusgeben(rückgabebetrag);      
    }
    
    public static double fahrkartenBestellungErfassen()
    {
    Scanner ms1 = new Scanner(System.in);
    double betrag;
    int	anzahl;
	System.out.print("Zu zahlender Betrag (EURO): ");
    betrag = ms1.nextDouble();
    
    System.out.print("Wie viele Tickets wollen sie Bestellen ?: ");	//Eingabe Anzahl der Tickets
    anzahl = ms1.nextInt();
    betrag=betrag*anzahl;
    return betrag;
    }
    
    public static double fahrkartenBezahlen(double betrag)
    {
    	double einzahlungsbetrag = 0.0;
    	double einzahlung;
    	Scanner ms2 = new Scanner(System.in);
        while(einzahlungsbetrag < betrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f %n", (betrag - einzahlungsbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   einzahlung = ms2.nextDouble();
     	   einzahlungsbetrag += einzahlung;
        }  

    	return einzahlungsbetrag;
    }
    
    public static void fahrkartenAusgeben(String text)
    {
    	 System.out.println(text);
         for (int i = 0; i < 50; i++) //i < default 8
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) 
   {
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO %n",rueckgeld);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	 rueckgeld -= 0.05;
            }
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.");
        }  
   }
}